package com.javarush.test.level08.lesson11.bonus01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.*;

/* Номер месяца
Программа вводит с клавиатуры имя месяца и выводит его номер на экран в виде: «May is 5 month».
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        //add your code here - напиши код тут
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String d = reader.readLine();
//        String[] month = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
//        for (int i=1; i<12; i++) {
//            if(d.toLowerCase().equals(month[i].toLowerCase())) System.out.println(month[i]+" is " + i + " month");
//        }
        Date date = new Date(d + "1 2006");
        System.out.println(d + " is " + (date.getMonth()+1) + " month");
    }
}