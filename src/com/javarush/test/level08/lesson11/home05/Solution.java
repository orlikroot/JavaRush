//package com.javarush.test.level08.lesson11.home05;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//
///* Мама Мыла Раму. Теперь с большой буквы
//Написать программу, которая вводит с клавиатуры строку текста.
//Программа заменяет в тексте первые буквы всех слов на заглавные.
//Вывести результат на экран.
//
//Пример ввода:
//  мама     мыла раму.
//
//Пример вывода:
//  Мама     Мыла Раму.
//*/
//
//public class Solution
//{
//    public static void main(String[] args) throws IOException
//    {
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        String s = reader.readLine();
//
//
//        //Напишите тут ваш код
//        s.replace(s.charAt(0), s.toUpperCase().charAt(0));
//        char[] y = s.toCharArray();
//        for (int i =1; i<y.length; i++) {
//           // s.charAt(0) = s.toLowerCase().charAt(0);
//            if (String.valueOf(y[i]).equals(" ") && String.valueOf(y[i+1]).equals("[^\\n\\t\\f\\r]")){ s.replace(s.charAt(i+1), s.toUpperCase().charAt(i+1));
////                String f = String.valueOf(y[i+1]).toUpperCase();
////                y[i+1] = Character.valueOf(f);}
//        }}
//        System.out.println(s);
//
////        String words[] = s.split(" ");
////        for (String x:words)
////            System.out.print(x.toUpperCase().charAt(0)+x.substring(1, x.length()) + " ");
//    }
//
//
//}
package com.javarush.test.level08.lesson11.home05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* Мама Мыла Раму. Теперь с большой буквы
Написать программу, которая вводит с клавиатуры строку текста.
Программа заменяет в тексте первые буквы всех слов на заглавные.
Пример ввода:
мама     мыла раму.
Пример вывода:
Мама     Мыла Раму.
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        char[] a = s.toCharArray();

        a[0] = Character.toUpperCase(a[0]);

        Boolean b = false;

        for (int i = 0; i < a.length; i++)
        {
            if (a[i] == ' ')
            {
                b = true;
            }
            else if (a[i] != ' ' && b)
            {
                a[i] = Character.toUpperCase(a[i]);
                b = false;
            }
        }

        System.out.println(a);
    }
}