//package com.javarush.test.level08.lesson11.home09;
//
//import java.util.Date;
//
///* Работа с датой
//1. Реализовать метод isDateOdd(String date) так, чтобы он
//возвращал true, если количество дней с начала года - нечетное число,
//иначе false
//2. String date передается в формате MAY 1 2013
//*/
//
//public class Solution
//{
//    public static void main(String[] args)
//    {
//        isDateOdd("MARCH 17 2015");
//    }
//
//    public static boolean isDateOdd(String date)
//    {
//        Date newYear = new Date();
//        newYear.setMonth(0); //устанавливаем месяц 0
//        newYear.setDate(1); //устанавливаем день
//
//        Date nowDate = new Date(date); //получаем переданную дату
//
//        //получаем валовое количество миллисекунд
//        long timeDistance = nowDate.getTime()-newYear.getTime();
//
//        long msDay = 24*60*60*1000;  //количество миллисекунд в одном дне
//
//        //делим валовое количество миллисекунд на миллисекунды в дне
//        int dayCount = (int)(timeDistance/msDay); //получаем кол-во дней
//
//        if (dayCount%2 != 0) return true;
//        else return false;
//    }
//}


package com.javarush.test.level08.lesson11.home09;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/* Работа с датой
1. Реализовать метод isDateOdd(String date) так, чтобы он возвращал true, если количество дней с начала года - нечетное число, иначе false
2. String date передается в формате MAY 1 2013
*/

public class Solution
{
    public static void main(String[] args) throws ParseException
    {
        String date = new String("MAY 1 2014");
        Date current = new SimpleDateFormat("MMMM dd yyyy", Locale.ENGLISH).parse(date);
        //System.out.println(current);
        isDateOdd(date);
    }

    public static boolean isDateOdd(String date) throws ParseException
    {
        SimpleDateFormat format = new SimpleDateFormat("MMM dd yyyy");
        Date yearBegin = new Date(114,0,1);
        Date dateNow = new Date(date);


        long difference = dateNow.getTime()  - yearBegin.getTime();
        long days = difference/(24*60*60*1000);



        System.out.println(format.format(yearBegin));
        System.out.println(format.format(dateNow));
        if(days % 2 != 0)
            return true;
        else return false;

    }
}