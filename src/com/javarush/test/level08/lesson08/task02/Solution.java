package com.javarush.test.level08.lesson08.task02;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* Удалить все числа больше 10
Создать множество чисел(Set<Integer>), занести туда 20 различных чисел.
Удалить из множества все числа больше 10.
*/

public class Solution
{
    public static HashSet<Integer> createSet()
    {
        //Напишите тут ваш код
        HashSet<Integer> set = new HashSet<Integer>();
        set.add(101);
        set.add(1102);
        set.add(1203);
        set.add(1304);
        set.add(1405);
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(1506);
        set.add(1607);
        set.add(1708);
        set.add(1809);
        set.add(9100);
        set.add(1007);
        set.add(1900);
        set.add(6106);
        set.add(5105);
        set.add(4104);
        set.add(3103);
        return set;

    }

    public static HashSet<Integer> removeAllNumbersMoreThan10(HashSet<Integer> set)
    {
        //Напишите тут ваш код
        Iterator<Integer> iterator = set.iterator();
        while (iterator.hasNext()){
            int a = iterator.next();
            if (a > 10)
                iterator.remove();
        }
        return set;
    }
    public static void main(String[] args) {
        //Solution.createSet();
        Solution.removeAllNumbersMoreThan10(createSet());
    }
}
