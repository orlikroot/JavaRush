package com.javarush.test.level08.lesson08.task01;

import java.util.HashSet;
import java.util.Set;

/* 20 слов на букву «Л»
Создать множество строк (Set<String>), занести в него 20 слов на букву «Л».
*/

public class Solution
{
    public static HashSet<String> createSet()
    {
        //Напишите тут ваш код
        HashSet<String> set = new HashSet<String>();
        set.add("Лира");
        set.add("Лираф");
        set.add("Лираы");
        set.add("Лирав");
        set.add("Лираа");
        set.add("Лирап");
        set.add("Лирар");
        set.add("Лирао");
        set.add("Лирал");
        set.add("Лирад");
        set.add("Лираж");
        set.add("Лираэ");
        set.add("Лирая");
        set.add("Лирач");
        set.add("Лирас");
        set.add("Лирам");
        set.add("Лириа");
        set.add("Лират");
        set.add("Лираь");
        set.add("Лираб");
        return set;
    }
}
