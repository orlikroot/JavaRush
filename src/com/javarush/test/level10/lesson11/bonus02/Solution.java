package com.javarush.test.level10.lesson11.bonus02;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

/* Нужно добавить в программу новую функциональность
Задача: Программа вводит с клавиатуры пару (число и строку) и выводит их на экран.
Новая задача: Программа вводит с клавиатуры пары (число и строку), сохраняет их в HashMap.
Пустая строка – конец ввода данных. Числа могу повторяться. Строки всегда уникальны. Введенные данные не должны потеряться!
Затем программа выводит содержание HashMap на экран.

Пример ввода:
1
Мама
2
Рама
1
Мыла

Пример вывода:
1 Мама
2 Рама
1 Мыла
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

//        int id = Integer.parseInt(reader.readLine());
//        String name = reader.readLine();
//
//        System.out.println("Id=" + id + " Name=" + name);
        String pair;
        HashMap<String, Integer> map = new HashMap();
        do
        {
            pair = reader.readLine();
            String[] sTrim = pair.trim().split("\\s");
            if (!pair.equals(""))
            {
                int s1 = Integer.parseInt(sTrim[0]);
                String s2 = sTrim[1];
                map.put(s2, s1);
            }
        }
        while (!pair.equals(""));
        for (HashMap.Entry<String, Integer> entry : map.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            System.out.println(value + " " + key);
        }
    }
}
