package com.javarush.test.level10.lesson11.home06;

/* Конструкторы класса Human
Напиши класс Human с 6 полями. Придумай и реализуй 10 различных конструкторов для него. Каждый конструктор должен иметь смысл.
*/

public class Solution
{
    public static void main(String[] args)
    {

    }

    public static class Human
    {
        //напишите тут ваши переменные и конструкторы
        int age = 20;
        boolean sex = true;
        String skin = "white";
        boolean children = true;
        boolean married = true;
        boolean driversLicense = false;

        Human(int age){
            this.age = age;
            boolean sex = true;
            String skin = "white";
            boolean children = true;
            boolean married = true;
            boolean driversLicense = false;
        }
        Human(int age, boolean sex) {
            this.age = age;
            this.sex = sex;
            String skin = "white";
            boolean children = true;
            boolean married = true;
            boolean driversLicense = false;
        }
        Human(int age, boolean sex, String skin) {
            this.age = age;
            this.sex = sex;
            this.skin = skin;
            boolean children = true;
            boolean married = true;
            boolean driversLicense = false;
        }
        Human(int age, boolean sex, String skin, boolean children) {
            this.age = age;
            this.sex = sex;
            this.skin = skin;
            this.children = children;
            boolean married = true;
            boolean driversLicense = false;
        }
        Human(int age, boolean sex, String skin, boolean children, boolean married) {
            this.age = age;
            this.sex = sex;
            this.skin = skin;
            this.children = children;
            this.married = married;
            boolean driversLicense = false;
        }
        Human(int age, boolean sex, String skin, boolean children, boolean married, boolean driversLicense) {
            this.age = age;
            this.sex = sex;
            this.skin = skin;
            this.children = children;
            this.married = married;
            this.driversLicense = driversLicense;
        }
        Human( boolean children, int age) {
            this.age = age;
            boolean sex = true;
            String skin = "white";
            this.children = children;
            boolean married = true;
            boolean driversLicense = false;
        }
        Human( boolean children, int age, String skin) {
            this.age = age;
            boolean sex = true;
            this.skin = skin;
            this.children = children;
            boolean married = true;
            boolean driversLicense = false;
        }
        Human( boolean children, int age, String skin, boolean married) {
            this.age = age;
            boolean sex = true;
            this.skin = skin;
            this.children = children;
            this.married = married;
            boolean driversLicense = false;
        }
        Human( boolean children, int age, String skin, boolean married, boolean driversLicense) {
            this.age = age;
            boolean sex = true;
            this.skin = skin;
            this.children = children;
            this.married = married;
            this.driversLicense = false;
        }
    }
}
