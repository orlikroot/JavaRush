package com.javarush.test.level10.lesson11.home08;

import java.util.ArrayList;

/* Массив списков строк
Создать массив, элементами которого будут списки строк. Заполнить массив любыми данными и вывести их на экран.
*/

public class Solution
{
    public static void main(String[] args)
    {
        ArrayList<String>[] arrayOfStringList =  createList();
        printList(arrayOfStringList);
    }

    public static ArrayList<String>[] createList()
    {
        //Напишите тут ваш код
        ArrayList<String> a1 = new ArrayList<>();
        ArrayList<String> a2 = new ArrayList<>();
        ArrayList<String> a3 = new ArrayList<>();


        ArrayList<String>[] arrayOfStringList = new ArrayList[]{a1, a2, a3};



        a1.add("rere");
        a1.add("dada");
        a1.add("gugu");

        a2.add("rere");
        a2.add("dada");
        a2.add("gugu");

        a3.add("rere");
        a3.add("dada");
        a3.add("gugu");

        arrayOfStringList[0] = a1;
        arrayOfStringList[1] = a2;
        arrayOfStringList[2] = a3;

        return arrayOfStringList;
    }

    public static void printList(ArrayList<String>[] arrayOfStringList)
    {
        for (ArrayList<String> list: arrayOfStringList)
        {
            for (String s : list)
            {
                System.out.println(s);
            }
        }
    }
}