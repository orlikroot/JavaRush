/*
 * Copyright (c) 2015. Krotov E.
 */

package com.javarush.test.level14.lesson08.home02;

/**
 * Created by Evgen on 03.08.2015.
 */
public abstract class Drink
{
    public void taste(){
        System.out.println("Вкусно");
    }
}
