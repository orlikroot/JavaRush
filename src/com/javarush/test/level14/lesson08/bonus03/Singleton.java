/*
 * Copyright (c) 2015. Krotov E.
 */

package com.javarush.test.level14.lesson08.bonus03;

/**
 * Created by Евгений on 12.10.2015.
 */
public class Singleton{

    private static Singleton instance;

    private Singleton(){}

    public static synchronized Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }

}
