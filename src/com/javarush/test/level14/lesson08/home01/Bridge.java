/*
 * Copyright (c) 2015. Krotov E.
 */

package com.javarush.test.level14.lesson08.home01;

/**
 * Created by Evgen on 03.08.2015.
 */
public interface Bridge

{
    public int getCarsCount();
}
