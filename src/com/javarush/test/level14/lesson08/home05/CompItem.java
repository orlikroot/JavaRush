/*
 * Copyright (c) 2015. Krotov E.
 */

package com.javarush.test.level14.lesson08.home05;

/**
 * Created by Evgen on 14.08.2015.
 */
public interface CompItem
{
    String getName();
}
