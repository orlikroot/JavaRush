/*
 * Copyright (c) 2015. Krotov E.
 */

package com.javarush.test.level14.lesson06.home01;

/**
 * Created by Evgen on 03.08.2015.
 */
public class RussianHen extends Hen
{
    @Override
    int getCountOfEggsPerMonth()
    {
        return 1;
    }



    public String getDescription(){
        return super.getDescription() + " Моя страна - "+ Country.RUSSIA + ". Я несу " + getCountOfEggsPerMonth() + " яиц в месяц.";
    }
}
