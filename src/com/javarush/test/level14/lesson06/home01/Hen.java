/*
 * Copyright (c) 2015. Krotov E.
 */

package com.javarush.test.level14.lesson06.home01;

/**
 * Created by Evgen on 03.08.2015.
 */
public abstract class Hen
{
    abstract int getCountOfEggsPerMonth();
    public String getDescription(){
        return "Я курица.";
    }
}
