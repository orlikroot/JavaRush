package com.javarush.test.level09.lesson11.home09;

import java.util.*;

/* Десять котов
Создать класс кот – Cat, с полем «имя» (String).
Создать словарь Map(<String, Cat>) и добавить туда 10 котов в виде «Имя»-«Кот».
Получить из Map множество(Set) всех имен и вывести его на экран.
*/

public class Solution
{
    public static void main(String[] args)
    {

        Map<String, Cat> map = createMap();
        Set<Cat> set = convertMapToSet(map);
        printCatSet(set);
    }

    public static Map<String, Cat> createMap()
    {
        //Напишите тут ваш код
        Map<String, Cat> map = new HashMap<String, Cat>();
        Cat bob1 = new Cat("BOB1");
        Cat bob2 = new Cat("BOB2");
        Cat bob3 = new Cat("BOB3");
        Cat bob4 = new Cat("BOB4");
        Cat bob5 = new Cat("BOB5");
        Cat bob6 = new Cat("BOB6");
        Cat bob7 = new Cat("BOB7");
        Cat bob8 = new Cat("BOB8");
        Cat bob9 = new Cat("BOB9");
        Cat bob10 = new Cat("BOB10");
        map.put(bob1.name, bob1);
        map.put(bob2.name, bob2);
        map.put(bob3.name, bob3);
        map.put(bob4.name, bob4);
        map.put(bob5.name, bob5);
        map.put(bob6.name, bob6);
        map.put(bob7.name, bob7);
        map.put(bob8.name, bob8);
        map.put(bob9.name, bob9);
        map.put(bob10.name, bob10);
        return map;

    }

    public static Set<Cat> convertMapToSet(Map<String, Cat> map)
    {
        //Напишите тут ваш код
        Set<Cat> set = new HashSet<Cat>();


        for (Cat val : map.values())
            set.add(val);
        return set;
    }

    public static void printCatSet(Set<Cat> set)
    {
        for (Cat cat:set)
        {
            System.out.println(cat);
        }
    }

    public static class Cat
    {
        private String name;

        public Cat(String name)
        {
            this.name = name;
        }

        public String toString()
        {
            return "Cat "+this.name;
        }
    }


}
