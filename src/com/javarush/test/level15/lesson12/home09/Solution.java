package com.javarush.test.level15.lesson12.home09;

/* Парсер реквестов
Считать с консоли URl ссылку.
Вывести на экран через пробел список всех параметров (Параметры идут после ? и разделяются &, например, lvl=15).
URL содержит минимум 1 параметр.
Если присутствует параметр obj, то передать его значение в нужный метод alert.
alert(double value) - для чисел (дробные числа разделяются точкой)
alert(String value) - для строк

Пример 1
Ввод:
http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo
Вывод:
lvl view name

Пример 2
Ввод:
http://javarush.ru/alpha/index.html?obj=3.14&name=Amigo
Вывод:
obj name
double 3.14
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class Solution
{
    public static void main(String[] args)
    {
        //add your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        URL aURL = null;
        try
        {
            aURL = new URL(reader.readLine());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        String[] keyParam = aURL.getQuery().split("&");
        String[] alertArrays = new String[2];
        for (String s : keyParam)
        {
            String[] keyParamSplit = s.split("=");
            System.out.print(keyParamSplit[0] + " ");
            if (keyParamSplit[0].equals("obj")) alertArrays = keyParamSplit;
        }
        System.out.println();
        if (alertArrays[1] != null)
            try {
                Double d = Double.parseDouble(alertArrays[1]);
                alert(d);
            } catch (Exception e) {
                alert(alertArrays[1]);
            }

            //alert(alertArrays[1]);
    }

    public static void alert(double value)
    {
        System.out.println("double " + value);
    }

    public static void alert(String value)
    {
        System.out.println("String " + value);
    }
}
