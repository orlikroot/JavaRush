/*
 * Copyright (c) 2015. Krotov E.
 */

package com.javarush.test.level15.lesson12.bonus01;

/**
 * Created by evgen on 26.10.2015.
 */
public class Plane implements Flyable
{

    private int count;
    @Override
    public void fly()
    {

    }

    public Plane(int count)
    {
        this.count = count;
    }
}
