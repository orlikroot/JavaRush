/*
 * Copyright (c) 2015. Krotov E.
 */

package com.javarush.test.level15.lesson12.bonus02;

/**
 * Created by Евгений on 27.10.2015.
 */
public abstract class DrinkMaker
{
    void getRightCup(){}
    void putIngredient(){}
    void pour(){}
    void makeDrink(){
        getRightCup();
        putIngredient();
        pour();
    }
}
