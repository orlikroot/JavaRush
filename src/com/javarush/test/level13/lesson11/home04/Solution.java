package com.javarush.test.level13.lesson11.home04;

/* Запись в файл
1. Прочесть с консоли имя файла.
2. Считывать строки с консоли, пока пользователь не введет строку "exit".
3. Вывести все строки в файл, каждую строчку с новой стороки.
*/

import java.io.*;
import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filePath = reader.readLine();
        ArrayList<String> text = new ArrayList<>();
        String textString;
        do {
            textString = "";
            textString = reader.readLine();
            text.add(textString + "\n" + "\r");

        } while (!textString.equals("exit"));
        FileOutputStream outputStream = new FileOutputStream(filePath);
        for (String textFromArray : text) {

                outputStream.write(textFromArray.getBytes());


        }
    }
}
