package com.javarush.test.level12.lesson04.task02;

/* print(int) и print(Integer)
Написать два метода: print(int) и print(Integer).
Написать такой код в методе main, чтобы вызвались они оба.
*/

import java.util.Objects;

public class Solution
{
    public static void main(String[] args)
    {
        new Solution().print(5);
        new Solution().print((Integer)4);
    }

    //Напишите тут ваши методы


    public void print(int a){
        System.out.println(a);
    }
    public void print(Integer a){
        System.out.println(a);
    }
}
