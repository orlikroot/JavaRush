package com.javarush.test.level07.lesson12.home03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* Максимальное и минимальное числа в массиве
Создать массив на 20 чисел. Заполнить его числами с клавиатуры. Найти максимальное и минимальное числа в массиве.
Вывести на экран максимальное и минимальное числа через пробел.
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int  maximum;
        int  minimum;

        //Напишите тут ваш код
        int[] s = new int[20];

        for (int i = 0; i<s.length; i++) s[i] = Integer.parseInt(reader.readLine());

        int[] z = (int[])s.clone();

        for (int i = 0; i<s.length-1; i++) {

            if (s[i] > s[i+1]) s[i+1] = s[i];
        }
        maximum = s[19];
        for (int i = 0; i<z.length-1; i++) {

            if (z[i] < z[i+1]) z[i+1] = z[i];
        }
        minimum = z[19];


        System.out.println(maximum + " " + minimum);
        //System.out.println(minimum);
    }
}
